
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    data: {
       name: null,
       email: null,
       rating: 'Please select a rating',
       comment: null,
       comments: [],
       averageRating: [],
       loaded: false,
       commentsInfo: null,
       commentsError: null,
       errors: []
    },
    mounted: function() {
      var self = this;
      this.getComments();

    },
    methods: {
      getComments: function() {

        this.commentsInfo = null;
        this.commentsError = null;

        axios.get('/api/comments').then((response) => {
          this.comments = response.data.comments;
          this.averageRating = response.data.average_rating;
        })
        .catch((error) => {
            this.commentsError = 'There was a problem with loading the comments.';
        });

        this.loaded = true;

      },
      postComment: function() {
        var formData = {
          name: this.name,
          email: this.email,
          rating: this.rating,
          comment: this.comment
        };

        axios.post('/api/comments/store', formData).then((response) => {
          this.clearForm();
          this.getComments();
          this.commentsInfo = 'Thank you. Your comment has been posted below.';
        })
        .catch((error) => {
            this.errors = error.response.data;
        });
      },
      clearForm: function() {
        this.name = '';
        this.email = '';
        this.rating = 'Please select a rating';
        this.comment = '';
      }
    }
});
