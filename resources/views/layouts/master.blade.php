<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Guestbook</title>
    <link rel="stylesheet" href="/css/app.css"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  </head>
  <body>
    <div class="container">
      <div class="content">
        <div id="app">
          @section('content')
            Input content here.
          @show
        </div>
      </div>
    </div>
    <script src="/js/app.js"></script>
  </body>
</html>
