@extends('layouts.master')

@section('content')

<h1> Guestbook Comments </h1>

<div id="add-comment">

    <div class="col-md-12">
      <p> * Required fields </p>

      <div v-if="errors.length > 0">
        <div class="alert alert-warning">
          <ul>
            <li v-for="(value, key) in errors">@{{ value }}</li>
          </ul>
        </div>
      </div>
    </div>

    <div class="col-md-4">
      <div class="form-field">
        <input type="text" v-model="name" placeholder="Enter your name*" class="form-control"/>
      </div>
    </div>

    <div class="col-md-4">
      <div class="form-field">
        <input type="email" v-model="email" placeholder="Enter your email address*" class="form-control"/>
      </div>
    </div>

    <div class="col-md-4">
      <div class="form-field">
        <select v-model="rating" class="form-control">
          <option val="0">Please select a rating</option>
          <option val="1">1</option>
          <option val="2">2</option>
          <option val="3">3</option>
          <option val="4">4</option>
          <option val="5">5</option>
        </select>
      </div>
    </div>

    <div class="col-md-12">
      <div class="form-field">
        <textarea rows="12" v-model="comment" placeholder="Enter your comment*" class="form-control"></textarea>
      </div>
    </div>

    <div class="col-md-12">
      <button class="btn btn-primary pull-right" @click="postComment"> Add comment </button>
    </div>

    {{ csrf_field() }}

</div>

<div class="col-md-12 comments">

  <div class="heading">
    <h2>Comments <span v-if="loaded && averageRating.length > 0" class="pull-right">Average Rating: @{{ averageRating }}</span></h2>
  </div>

  <p v-if="!loaded"> Guestbook comments are loading... </p>

  <div class="clear"></div>

  <div v-if="commentsError">
    <div class="alert alert-warning">@{{ commentsError }}</div>
  </div>

  <div v-if="commentsInfo">
    <div class="alert alert-success">@{{ commentsInfo }}</div>
  </div>

  <div v-if="comments.length > 0 && loaded">
      <div class="col-md-12 card" v-for="comment in comments">
        <p> Posted by: <strong>@{{ comment.name }}</strong> (@{{ comment.email }}) on <strong> @{{ comment.created_at }} </strong> </p>

        <p v-if="comment.rating"> Rating: <strong> @{{ comment.rating }} Stars </strong> </p>

        <p> @{{ comment.body }} </p>
      </div>
  </div>

  <div v-else-if="comments.length == 0 && loaded == true">
     <p> No comments have appeared to be posted to the guestbook yet. Be <a href="#add-comment">the first</a>! </p>
  </div>
</div>

@stop
