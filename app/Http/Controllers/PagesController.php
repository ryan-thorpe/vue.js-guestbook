<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;

class PagesController extends Controller
{
    public function index()
    {
       return view('pages.comments.index');
    }
}
