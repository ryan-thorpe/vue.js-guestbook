<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Validator;
use App\Comment;

class CommentsController extends Controller
{
  public function get()
  {
     $comments = Comment::latest()->get();
     $averageRating = round($comments->avg('rating'), 1);

     return ['comments' => $comments, 'average_rating' => $averageRating];
  }

  public function store(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'name' => 'required',
      'email' => 'required|email',
      'comment' => 'required'
    ]);

    if($validator->fails())
    {
       $errors = [];

       foreach($validator->errors()->all() as $key => $value)
       {
           $errors[] = $value;
       }

       return response()->json($errors, 422);
    }

     $comment = new Comment;
     $comment->name = $request->name;
     $comment->email = $request->email;

     if($request->rating == "Please select a rating")
     {
        $comment->rating = null;
     }
     else
     {
       $comment->rating = $request->rating;
     }

     $comment->body = $request->comment;
     $comment->save();
  }
}
