<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \Carbon\Carbon;

class Comment extends Model
{
    protected $fillable = ['name, comment, rating'];

    public function getCreatedAtAttribute($date)
    {
       return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d/m/Y H:i');
    }
}
