<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@index')->name('index');

Route::group(['prefix' => 'api'], function() {
    Route::get('/comments/', 'CommentsController@get');
    Route::post('/comments/store', 'CommentsController@store');
});
